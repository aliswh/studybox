package com.study.StudyBox

import java.io.Serializable

class Lobby() : Serializable {
    lateinit var title: String
    lateinit var category: String
    lateinit var description: String
    lateinit var timestamp: String
    var color = R.color.colorAccent
}