package com.study.StudyBox

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val TAG = "MainActivity"
    private lateinit var mAuth: FirebaseAuth
    private var currentUser: FirebaseUser? = null
    private var bluetoothAdapter: BluetoothAdapter? = null
    private val REQUEST_ENABLE_BT = 1
    private val REQUEST_LOBBY_CREATION = 1

    //detecting bluetooth state changes
    private val mReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
                val state = intent.getIntExtra(
                    BluetoothAdapter.EXTRA_STATE,
                    BluetoothAdapter.ERROR
                )
                when (state) {
                    BluetoothAdapter.STATE_OFF -> {val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)}
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Initialize Firebase
        mAuth = FirebaseAuth.getInstance()

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        //check bluetooth support on current device
        if(bluetoothAdapter==null){
            //Bluetooth is not supported!
            Log.d(TAG, "Bluetooth not supported on this device!")
            Toast.makeText(this, "Your device does not support bluetooth, thus "+R.string.app_name.toString()+" won't work properly!", Toast.LENGTH_LONG).show()
            finish()
        }

        //check if bluetooth is enabled
        if(bluetoothAdapter?.isEnabled==false){
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        }

        registerReceiver(mReceiver, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))

        //Drawer menu setup
        setSupportActionBar(toolbar)
        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigatino_drawer_open, R.string.navigatino_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        //setting default fragment
        if(savedInstanceState==null){
            val frag = SearchFragment()
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container, frag)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .show(frag)
                .commit()
            nav_view.setCheckedItem(R.id.action_user)
        }
    }

    override fun onBackPressed() {
        if(drawer_layout.isDrawerOpen(GravityCompat.START)){
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        else
            super.onBackPressed()
    }

    override fun onStart() {
        super.onStart()
        currentUser = mAuth.currentUser
        if(currentUser==null){
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(mReceiver)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==REQUEST_ENABLE_BT){
            if(resultCode== Activity.RESULT_OK) {
                Log.d(TAG, "Bluetooth enabled!")
                Toast.makeText(this, "Bluetooth enabled!", Toast.LENGTH_SHORT).show()
            }
            else {
                Log.d(TAG, "Bluetooth not enabled!")
                Toast.makeText(this, "Your device does not support bluetooth, thus "+R.string.app_name+" won't work properly!", Toast.LENGTH_LONG).show()
                finish()
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.action_user ->  supportFragmentManager.beginTransaction().replace(R.id.fragment_container, UserFragment()).commit()
            R.id.action_settings ->  supportFragmentManager.beginTransaction().replace(R.id.fragment_container, GearFragment()).commit()
            R.id.action_search ->  supportFragmentManager.beginTransaction().replace(R.id.fragment_container, SearchFragment()).commit()
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


    fun signOut(v: View) {
        mAuth.signOut()
        currentUser = null
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

}
