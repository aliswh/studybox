package com.study.StudyBox

import DividerItemDecoration
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import kotlinx.android.synthetic.main.fragment_search.*
import java.util.*
import kotlin.collections.ArrayList


class SearchFragment: Fragment(), SwipeRefreshLayout.OnRefreshListener ,LobbyAdapter.Companion.LobbyAdapterListener {

    private val lobbies = ArrayList<Lobby>()
    var actionMode:ActionMode? = null
    var actionModeCallback = ActionModeCallback()
    val REQUEST_LOBBY_CREATION = 2
    lateinit var mAdapter:LobbyAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mAdapter = LobbyAdapter(activity!!.baseContext, lobbies, this)

        val mLayoutManager = LinearLayoutManager(activity!!.applicationContext)
        recycler_view.layoutManager = mLayoutManager
        recycler_view.adapter = mAdapter
        recycler_view.itemAnimator = DefaultItemAnimator()
        recycler_view.addItemDecoration(DividerItemDecoration(activity!!.baseContext, LinearLayoutManager.VERTICAL))
        swipe_refresh_layout.setOnRefreshListener(this)
        val lobby1 = Lobby()
        lobby1.title = "PROVA"
        lobby1.category ="Test"
        lobby1.description ="xxxx"
        lobby1.timestamp="12-12-12"
        lobbies.add(lobby1)
        mAdapter.notifyDataSetChanged()

        fab.setOnClickListener {
            val intent = Intent(activity, CreateLobbyActivity::class.java)
            startActivityForResult(intent, REQUEST_LOBBY_CREATION)
        }
    }

    override fun onIconClicked(position: Int) {
        if (actionMode == null) {
            actionMode = activity?.startActionMode(actionModeCallback);
        }
        toggleSelection(position);
    }

    override fun onIconImportantClicked(position: Int) {
        // Star icon is clicked,
        // mark the message as important
        // Star icon is clicked,
        // mark the message as important
        val lobby: Lobby = lobbies.get(position)
        lobbies.set(position, lobby)
        mAdapter?.notifyDataSetChanged()
    }

    override fun onMessageRowClicked(position: Int) {
        // verify whether action mode is enabled or not
        // if enabled, change the row state to activated
        // verify whether action mode is enabled or not
        // if enabled, change the row state to activated
        if (mAdapter?.selectedItemCount!! > 0) {
            enableActionMode(position)
        } else {
            // read the message which removes bold from the row
            val lobby: Lobby = lobbies.get(position)
            lobbies.set(position, lobby)
            mAdapter?.notifyDataSetChanged()
        }
    }

    override fun onRowLongClicked(position: Int) {
        enableActionMode(position);
    }
    fun enableActionMode(position: Int) {
        if (actionMode == null) {
            actionMode = activity?.startActionMode(actionModeCallback)
        }
        toggleSelection(position)
    }

    private fun toggleSelection(position: Int) {
        mAdapter?.toggleSelection(position)
        val count = mAdapter?.selectedItemCount
        if (count == 0) {
            actionMode?.finish()
        } else {
            actionMode?.title = count.toString()
            actionMode?.invalidate()
        }
    }


    inner class ActionModeCallback : ActionMode.Callback {
        override fun onCreateActionMode(mode: ActionMode, menu: Menu?): Boolean {
            mode.menuInflater.inflate(R.menu.menu_action_mode, menu)

            // disable swipe refresh if action mode is enabled
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
            return false
        }

        override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
            return when (item.itemId) {
                R.id.action_delete -> {
                    // delete all the selected messages
                    deleteMessages()
                    mode.finish()
                    true
                }
                else -> false
            }
        }

        override fun onDestroyActionMode(mode: ActionMode) {
            mAdapter?.clearSelections()
            swipe_refresh_layout.setEnabled(true)
            actionMode = null
            recycler_view.post(Runnable {
                mAdapter?.resetAnimationIndex()
                // mAdapter.notifyDataSetChanged();
            })
        }
    }

    private fun deleteMessages() {
        mAdapter.resetAnimationIndex()
        val selectedItemPositions = mAdapter.getSelectedItems()
        Log.d("PROVAAA", "prima era ${lobbies.get(0).title}")
        for(i in selectedItemPositions){
            Log.d("PROVAAA", "prima era ${lobbies.get(i).title}")
            lobbies.removeAt(i)
            Log.d("PROVAAA", "indirizzo: $i, valore = ${lobbies.get(0).title}")
        }
        mAdapter.notifyDataSetChanged()
    }



    override fun onRefresh() {
        Toast.makeText(activity!!.baseContext, "Refreshed!", Toast.LENGTH_SHORT).show()
        swipe_refresh_layout.isRefreshing = false

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==REQUEST_LOBBY_CREATION){
            if(resultCode== Activity.RESULT_OK) {
                Log.d("PROVAAA", "result ok")
                val bundle = data?.extras
                val lobby = bundle!!.getSerializable("Lobby") as Lobby
                lobbies.add(lobby)
                mAdapter.notifyDataSetChanged()
            }
        }
    }
}