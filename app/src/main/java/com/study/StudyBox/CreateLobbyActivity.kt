package com.study.StudyBox

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_create_lobby.*
import kotlinx.android.synthetic.main.cardview.*
import java.text.SimpleDateFormat
import java.util.*

class CreateLobbyActivity : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_lobby)

        val adapter = ArrayAdapter.createFromResource(this, R.array.categories, android.R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
    }

    fun createLobby(v: View){
        val lobby = Lobby()
        lobby.title = lobby_name.text.toString()
        lobby.description = lobby_dtext.text.toString()
        lobby.category = spinner.selectedItem.toString()
        lobby.color = getRandomMaterialColor()
        val date = Date(System.currentTimeMillis())
        val dataFormat = SimpleDateFormat("hh:mm aa", Locale.ITALIAN)
        lobby.timestamp = dataFormat.format(date)

        Log.d("PROVAAA", "${lobby.title}, ${lobby.description}, ${lobby.category}")
        val intent = Intent()
        intent.putExtra("Lobby", lobby)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun getRandomMaterialColor(): Int {
        var returnColor: Int = Color.GRAY
        val rnd = Random()
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)).toInt()
    }


}